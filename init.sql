DROP DATABASE simplon;
CREATE DATABASE simplon;
USE simplon;

CREATE TABLE `apprenant` (
  `id` int PRIMARY KEY,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL
);

CREATE TABLE `groupe` (
  `id_group` int,
  `id_apprenant` int,
  PRIMARY KEY (`id_group`, `id_apprenant`)
);

CREATE TABLE `exercice` (
  `id` int PRIMARY KEY,
  `nom` varchar(50) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL
);

CREATE TABLE `competence` (
  `id` int,
  `libelle` varchar(50) NOT NULL,
  `id_diplome` int,
  PRIMARY KEY (`id`, `id_diplome`)
);

CREATE TABLE `diplomes` (
  `id` int PRIMARY KEY,
  `libelle` varchar(50) NOT NULL
);

CREATE TABLE `group_comp_exo` (
  `group_id` int,
  `competence_id` int,
  `exercice_id` int,
  `valide` bool,
  PRIMARY KEY (`group_id`, `competence_id`, `exercice_id`)
);

CREATE TABLE `diplome_apprenant` (
  `id_diplome` int,
  `id_apprenant` int,
  `valide` bool,
  `date_de_passage` date,
  PRIMARY KEY (`id_diplome`, `id_apprenant`)
);

CREATE TABLE `materiels` (
  `id` int PRIMARY KEY,
  `id_apprenant` int NOT NULL,
  `libelle` varchar(50) NOT NULL
);

CREATE TABLE `absence_apprenant` (
  `id_apprenant` int,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  PRIMARY KEY (`id_apprenant`, `date_debut`, `date_fin`)
);

ALTER TABLE `groupe` ADD FOREIGN KEY (`id_apprenant`) REFERENCES `apprenant` (`id`);
ALTER TABLE `group_comp_exo` ADD FOREIGN KEY (`group_id`) REFERENCES `groupe` (`id_group`);
ALTER TABLE `group_comp_exo` ADD FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`);
ALTER TABLE `group_comp_exo` ADD FOREIGN KEY (`exercice_id`) REFERENCES `exercice` (`id`);
ALTER TABLE `diplome_apprenant` ADD FOREIGN KEY (`id_diplome`) REFERENCES `diplomes` (`id`);
ALTER TABLE `diplome_apprenant` ADD FOREIGN KEY (`id_apprenant`) REFERENCES `apprenant` (`id`);
ALTER TABLE `materiels` ADD FOREIGN KEY (`id_apprenant`) REFERENCES `apprenant` (`id`);
ALTER TABLE `absence_apprenant` ADD FOREIGN KEY (`id_apprenant`) REFERENCES `apprenant` (`id`);

INSERT INTO `apprenant` (`id`, `nom`, `prenom`, `date_debut`, `date_fin`) VALUES
(1, 'Dupont', 'Jean', '2023-01-01', '2023-12-31'),
(2, 'Martin', 'Alice', '2023-01-01', '2023-12-31'),
(3, 'Moreau', 'Luc', '2023-01-01', '2023-12-31'),
(4, 'Lefebvre', 'Marie', '2023-01-01', '2023-12-31'),
(5, 'Garcia', 'Ana', '2023-01-01', '2023-12-31'),
(6, 'Rousseau', 'Julien', '2023-01-01', '2023-12-31'),
(7, 'Blanc', 'Sophie', '2023-01-01', '2023-12-31'),
(8, 'Girard', 'Thomas', '2023-01-01', '2023-12-31');

-- Insertion des données dans la table `groupe`
INSERT INTO `groupe` (`id_group`, `id_apprenant`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8);

INSERT INTO `exercice` (`id`, `nom`, `date_debut`, `date_fin`) VALUES
(1, 'Exercice 1', '2023-01-01', '2023-01-15'),
(2, 'Exercice 2', '2023-01-16', '2023-01-31'),
(3, 'Exercice 3', '2023-02-01', '2023-02-15'),
(4, 'Exercice 4', '2023-02-16', '2023-02-28'),
(5, 'Exercice 5', '2023-03-01', '2023-03-15'),
(6, 'Exercice 6', '2023-03-16', '2023-03-31');

INSERT INTO `competence` (`id`, `libelle`, `id_diplome`) VALUES
(1, 'Compétence 1', 1),
(2, 'Compétence 2', 2),
(3, 'Compétence 3', 3),
(4, 'Compétence 4', 4),
(5, 'Compétence 5', 5),
(6, 'Compétence 6', 6);

INSERT INTO `diplomes` (`id`, `libelle`) VALUES
(1, 'Diplôme 1'),
(2, 'Diplôme 2'),
(3, 'Diplôme 3'),
(4, 'Diplôme 4'),
(5, 'Diplôme 5'),
(6, 'Diplôme 6');

INSERT INTO `group_comp_exo` (`group_id`, `competence_id`, `exercice_id`, `valide`) VALUES
(1, 1, 1, TRUE),
(2, 2, 2, FALSE),
(3, 3, 3, TRUE),
(4, 4, 4, FALSE),
(5, 5, 5, TRUE),
(6, 6, 6, FALSE);

INSERT INTO `diplome_apprenant` (`id_diplome`, `id_apprenant`, `valide`, `date_de_passage`) VALUES
(1, 1, TRUE, '2023-12-31'),
(2, 2, FALSE, '2023-12-31'),
(3, 3, TRUE, '2023-12-31'),
(4, 4, FALSE, '2023-12-31'),
(5, 5, TRUE, '2023-12-31'),
(6, 6, FALSE, '2023-12-31');

INSERT INTO `materiels` (`id`, `id_apprenant`, `libelle`) VALUES
(1, 1, 'Ordinateur'),
(2, 2, 'Tablette'),
(3, 3, 'Souris'),
(4, 4, 'Écouteurs'),
(5, 5, 'Tableau interactif'),
(6, 6, 'Projecteur'),
(7, 7, 'Caméra'),
(8, 8, 'Microphone');

INSERT INTO `absence_apprenant` (`id_apprenant`, `date_debut`, `date_fin`) VALUES
(1, '2023-02-01', '2023-02-10'),
(2, '2023-03-01', '2023-03-10'),
(3, '2023-04-01', '2023-04-10'),
(4, '2023-02-01', '2023-02-10'),
(5, '2023-03-01', '2023-03-10'),
(6, '2023-04-01', '2023-04-10'),
(7, '2023-05-01', '2023-05-10'),
(8, '2023-06-01', '2023-06-10');
