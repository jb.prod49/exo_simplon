# Les questions et réponse

## liste des apprenants

```sql
SELECT * FROM apprenant;
```



| id | nom      | prenom | date_debut | date_fin   |
| -- | -------- | ------ | ---------- | ---------- |
| 1  | Dupont   | Jean   | 2023-01-01 | 2023-12-31 |
| 2  | Martin   | Alice  | 2023-01-01 | 2023-12-31 |
| 3  | Moreau   | Luc    | 2023-01-01 | 2023-12-31 |
| 4  | Lefebvre | Marie  | 2023-01-01 | 2023-12-31 |
| 5  | Garcia   | Ana    | 2023-01-01 | 2023-12-31 |
| 6  | Rousseau | Julien | 2023-01-01 | 2023-12-31 |
| 7  | Blanc    | Sophie | 2023-01-01 | 2023-12-31 |
| 8  | Girard   | Thomas | 2023-01-01 | 2023-12-31 |

## nombre de compétence validée par apprenant

```sql
SELECT da.id_apprenant, COUNT(*) AS nombre_competences_validees
FROM diplome_apprenant da
WHERE da.valide = TRUE
GROUP BY da.id_apprenant;

```

## nombre d'absence (jours) par apprenants

```sql
SELECT id_apprenant, SUM(DATEDIFF(date_fin, date_debut) + 1) AS nombre_jours_absence
FROM absence_apprenant
GROUP BY id_apprenant;

```

## nombre d'absence (jours) et nombre de compétence validée

```sql
SELECT a.id_apprenant, 
       SUM(DATEDIFF(aa.date_fin, aa.date_debut) + 1) AS nombre_jours_absence, 
       COUNT(da.id_apprenant) AS nombre_competences_validees
FROM apprenant a
LEFT JOIN absence_apprenant aa ON a.id = aa.id_apprenant
LEFT JOIN diplome_apprenant da ON a.id = da.id_apprenant AND da.valide = TRUE
GROUP BY a.id_apprenant;

```

## tout les apprenants d'un groupe

```sql
SELECT a.*
FROM apprenant a
JOIN groupe g ON a.id = g.id_apprenant
WHERE g.id_group = 1;

```

## nombre de groupe auxquels chaque apprenant a participé



## pour 1 apprenant (avec son nom) la liste des compétences et leur état (validé ou pas)

```sql
SELECT id_apprenant, COUNT(*) AS nombre_groupes
FROM groupe
GROUP BY id_apprenant;

```

## le plus absent qui doit ramener des pains au chocolat.



## pour une date donnée la liste des présents.



## liste des apprenants avec leur ordinateur / matériel



## pour un projet la liste des apprenant et leur groupe



## pour chaque compétence le % de validation (si vous avez mis des niveaux, le niveau 3)
